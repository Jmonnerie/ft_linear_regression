# Ft_linear_regression

This 42 school project is an introduction to machine learning.

## Goal

For this project, we had to create a program that predicts the price of a car
by using a linear function train with a gradient descent algorithm.

## Install and launch

`make install` will install all dependencies. It performs a "pip3 install -r requirements.txt"
`make train` will train the app with the dataset ("./data/data.csv")
`make visu_train` will train and launch the visualisator to see what happend in the algorithm
`make price_of [mileage]` will get the price of a car with mileage passed in parameter
`make tests` will launch the unit tests of this project
