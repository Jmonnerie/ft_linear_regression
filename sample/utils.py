# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    utils.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/01 07:26:38 by jojomoon          #+#    #+#              #
#    Updated: 2020/07/28 11:28:54 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import math

def findTheBiggerIdx(tab):
    """ Relevant """
    idx_max = 0
    for idx in range(0, len(tab)):
        if tab[idx] > tab[idx_max]:
            idx_max = idx
    return idx_max

def is_number(s):
    """ Is the number passed in argument a real number ? """
    try:
        num = float(s)
        if math.isnan(num) or math.isinf(num):
            raise ValueError()
        return True
    except:
        return False