# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    visu.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/31 20:02:10 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/10 04:10:51 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import matplotlib.pyplot as plt
import sys
import parse
import strings
import train
import priceOf
from exceptions import UsageError, ValueOutOfRange, ValueNotGood

def displayGraph(ds, t0, t1):
  plt.clf()
  plt.xlabel(ds.nameX)
  plt.ylabel(ds.nameY)
  valX = [x * ds.divider for x in ds.valuesX]
  valY = [x * ds.divider for x in ds.valuesY]
  plt.plot(valX, valY, 'o')
  rangeY = [t0, priceOf.getPriceOf(t0, t1, ds.xMax)]
  plt.plot(ds.rangeX, rangeY)
  plt.pause(0.0001)

def main(ds):
  ds.setIdxMax()
  ds.setRangeX()
  plt.ion()
  train.main(ds, True)
  plt.ioff()
  plt.show()
  

if __name__ == "__main__":
  try:
    ds = parse.main()
    main(ds)
  except FloatingPointError:
    print(strings.valueOutOfRange)
    sys.exit(1)
  except UsageError:
    print(strings.visuUsage)
    sys.exit(1)
  except ValueOutOfRange:
    print(strings.valueOutOfRange)
    sys.exit(1)
  except ValueNotGood:
    print(strings.valuesNotGood)
    sys.exit(1)