# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    train.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/31 20:11:01 by jojomoon          #+#    #+#              #
#    Updated: 2020/07/28 11:25:40 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

"""
    This module trains the model with a dataset, passed in argument or data.csv by default

    :raise FloatingPointError: when a value is too big to be calculated
    :raise UsageError: When the module is not well called or with not good arguments
    :raise:
"""

import parent_import
import sys
import os
import strings
import parse
import visu
import utils
from exceptions import UsageError, ValueOutOfRange


def save(t0, t1):
    if os.path.exists(strings.thetaValuesFile):
        os.remove(strings.thetaValuesFile)
    fd = open(strings.thetaValuesFile, "w+")
    fd.write("theta0=%f\ntheta1=%f" % (t0, t1))
    fd.close()


def sumTmpT0(ds, t0, t1):
    # somme des différences entre la valeur théorique et réelle
    sum = t0 * ds.len + t1 * ds.sumX - ds.sumY
    return sum


def sumTmpT1(ds, t0, t1):
    # somme du kilométrage multiplié par les différences entre la valeur théorique et réelle
    sum = t0 * ds.sumX + t1 * ds.sumXxX - ds.sumXxY
    return sum


def main(ds, visual):
    if type(visual) != bool:
        raise TypeError()
    t0 = 0.0
    t1 = 0.0
    learningRate = ds.learningRate
    i = 0
    while True:
        try:
            sumT0 = sumTmpT0(ds, t0, t1)
            sumT1 = sumTmpT1(ds, t0, t1)
            if not utils.is_number(sumT0) or not utils.is_number(sumT1):
                raise FloatingPointError()
        except FloatingPointError:
            ds.updateDivider()
            t0, t1 = 0.0, 0.0
            continue
        tmpT0 = learningRate * sumT0 / ds.len
        tmpT1 = learningRate * sumT1 / ds.len
        if abs(tmpT0) < float(0.000001) and abs(tmpT1) < float(0.000001):
            save(t0 * ds.divider, t1)
            return
        if visual:
            if i == 1000:
                i = 0
                visu.displayGraph(ds, t0 * ds.divider, t1)
            else:
                i += 1
        t0 -= tmpT0
        t1 -= tmpT1


if __name__ == "__main__":
    try:
        ds = parse.main()
        main(ds, False)
    except UsageError:
        print(strings.trainUsage)
        sys.exit(1)
    except ValueOutOfRange:
        print(strings.valueOutOfRange)
        sys.exit(1)
    except TypeError:
        print(strings.dsIsnotDataset)
        sys.exit(1)