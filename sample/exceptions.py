# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    exceptions.py                                      :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/28 04:14:53 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/09 06:21:01 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

class UsageError(Exception): pass

class ThetaValuesCorrupted(Exception): pass

class ValueOutOfRange(Exception): pass

class ValueNotGood(Exception): pass

class DatasetNotEqual(Exception): pass