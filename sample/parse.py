# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    parse.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/31 21:21:02 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/09 06:03:32 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

"""
	Parse files passed in argv or ./data/data.csv if no arguments

	The file has to be the same as the ones in example.
	If multiple files are sended in argument, their names have to be the same, or a Usage error will be launched

	Example:
	data1.csv
		km,price
		240000,3650
		139800,3800
		150500,4400
		185530,4450
	data2.csv
		km, price
		176000,5250
		114800,5350
		166800,5800
		89000,5990
		144500,5999
	returned object:
		nameX = km, nameY = price
		valuesX = [ 240000, 139800, 150500, 185530, 176000, 114800, 166800, 89000, 144500 ]
		valuesY = [ 3650, 3800, 4400, 4450, 5250, 5350, 5800, 5990, 5999 ]

	:raise UsageError: When problem encountered on the argment files
	:return: A dataset object

	.. seealso:: dataset.py
"""

import sys
import os
import strings
import utils
from dataset import Dataset
from exceptions import UsageError 

def getFileData(fileName):
	"""
		Parse informations of the file in parameter
	
		:param fileName: Strings: the path of file to parse
	
		:return valX: array of values X with his name (km in example) at index 0
		:return valX: array of values Y with his name (price in example) at index 0
		
		.. seealso:: help(parse.py)
	"""
	km = []
	price = []
	# Open the file and read the lines
	if not os.path.exists(fileName):
		print(strings.fileNotFound % fileName)
		raise UsageError()
	fd = open(fileName, 'r') 
	lines = fd.readlines()
	fd.close()
	# Get the value names (first line)
	names = lines[0].split(",")
	if len(names) != 2:
		print(strings.nameError % fileName)
		raise UsageError()
	km.append(names[0])
	price.append(names[1].replace("\n", ""))
	# Get values
	for idx in range(1, len(lines)):
		values = lines[idx].split(",")
		# Check line content
		try:
			valX = float(values[0].replace(" ", ""))
			valY = float(values[1].replace(" ", ""))
		except:
			print(strings.parseValueError % (fileName, idx + 1))
			raise UsageError()
		if not utils.is_number(valX) or not utils.is_number(valY):
			print(strings.numberNotReal % (fileName, idx + 1))
			raise UsageError()
		km.append(valX)
		price.append(valY)
	return km, price

def main():
	"""
		Iterates through the files in argv and launch the getFileData function for each.
	
		:return: A dataset object filled with compiled datas of all argument files
		
		.. seealso:: help(parse.py)
	"""
	km = []
	price = []
	for file in sys.argv[1:]:
		# If the argument is not a file, continue
		if file == "visu" or file == "train":
			continue
		# Get datas from the file
		tmpKm, tmpPrice = getFileData(file)
		if len(km) > 0:
			# Test if names are the same through files before append
			if km[0] == tmpKm[0] and price[0] == tmpPrice[0]:
				km += tmpKm[1:]
				price += tmpPrice[1:]
			else:
				print(strings.valueNamesNotEqual % (file, km[0], price[0], tmpKm[0], tmpPrice[0]))
				raise UsageError()
		# Here km and price are empty
		else:
			km = tmpKm
			price = tmpPrice
	# No data files in argument. Use default data file.
	if len(km) == 0:
		km, price = getFileData(strings.defaultFile)
	ds = Dataset(km[1:], price[1:], km[0], price[0])
	return ds