# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    dataset.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/04/04 01:16:37 by jojomoon          #+#    #+#              #
#    Updated: 2020/07/28 11:34:37 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

"""
    This module contains the dataset object, needed by train, visu, main and parse modules

    .. seealso:: main.py, visu.py, train.py, parse.py
"""

import numpy as np
import utils
import math
from exceptions import ValueNotGood, ValueOutOfRange, DatasetNotEqual

np.seterr(all="raise")


class Dataset(object):
    """
        This object contains information about linear regression's dataset. They are composed of 2 values, X and Y

        It takes 2 arrays, valX and valY, and their names, nameX and nameY.
        In some datasets, values are too big to be calculated, and Dataset's methods can raise a FloatingPointError.

        :param valuesX: Array of values X (mileage in the example above)
        :param valuesY: Array of values Y (price in the example above)
        :param nameX: Name of valX (mileage in the example above)
        :param nameX: Name of valY (price in the example above)
        :ivar valuesX: Array of valuesX divided by "divider" var in settings.py
        :ivar valuesY: Array of valuesY divided by "divider" var in settings.py
        :ivar len: Length of the dataset
        :ivar sumX: Sum of all values in valuesX
        :ivar sumY: Sum of all values in valuesY
        :ivar sumXxX: Sum of all valuesX * valuesX
        :ivar sumXxY: Sum of all valuesX * valuesY
        :raise ValueOutOfRange: if values are too big
        :raise ValueNotGood: If values in dataset are not numbers
        :raise DatasetNotEqual: If len(valX) != len(valY)

        :Example:
        ``` shell
            $ cat data.csv  # dataset of price of a car in fonction of his mileage:
            mileage,price
            240000,3650
            139800,3800
            150500,4400
            185530,4450
            176000,5250
            $ python3.7
            >>> mileage = [240000, 139800, 150500, 185530, 176000]
            >>> price = [3650, 3800, 4400, 4450, 5250]
            >>> import dataset
            >>> ds = dataset.Dataset(mileage, price, "mileage", "price")
            >>> print(ds)
            Dataset price in founction of mileage (5 values):- mileage: [240000.0, 139800.0, 150500.0, 185530.0, 176000.0]
            - price: [3650.0, 3800.0, 4400.0, 4450.0, 5250.0]
            - sumX: 891830
            - sumY: 21550
            - sumXxX: 165191670900
            - sumXxY: 3819048500
        ```
    """

    def __init__(self, valX, valY, nameX, nameY):
        self.len = len(valX)
        if self.len != len(valY):
            raise DatasetNotEqual()
        self.divider = 1
        self.learningRate = 0.0001
        self.nameX = nameX
        self.nameY = nameY
        for i in range(0, self.len):
            x = utils.is_number(valX[i])
            y = utils.is_number(valY[i])
            if not x or not y:
                raise ValueNotGood()
        self.valuesX = [float(valX[i]) for i in range(0, self.len)]
        self.valuesY = [float(valY[i]) for i in range(0, self.len)]
        try:
            self.sumX = np.sum(self.valuesX)
            self.sumY = np.sum(self.valuesY)
            self.sumXxY = np.sum([self.valuesX[i]*self.valuesY[i] for i in range(0, self.len)])
            self.sumXxX = np.sum([self.valuesX[i]*self.valuesX[i] for i in range(0, self.len)])
            if self.someoneIsInf():
                raise ValueOutOfRange()
        except:
            self.updateDivider()

    def __str__(self):
        return "Dataset %s in founction of %s (%d values):\n" % (self.nameY, self.nameX, self.len)+\
               "- %s: %s\n" % (self.nameX, self.valuesX)+\
               "- %s: %s\n" % (self.nameY, self.valuesY)+\
               "- %s: %s\n" % ("sumX", self.sumX)+\
               "- %s: %s\n" % ("sumY", self.sumY)+\
               "- %s: %s\n" % ("sumXxX", self.sumXxX)+\
               "- %s: %s" % ("sumXxY", self.sumXxY)

    def setIdxMax(self):
        """
            Set idxMax to the index in the dataset who represents the bigger value
        """
        self.idxMax = utils.findTheBiggerIdx(self.valuesX)
        self.xMax = self.valuesX[self.idxMax]

    def setRangeX(self):
        """
            Set an array of 2 values [0, xMax] needed by the visualisator to draw the linear function
        """
        try:
            self.xMax
        except:
            self.setIdxMax()
        self.rangeX = [0, self.xMax]

    def updateDivider(self):
        """
            Update the divider value to get the dataset values lower.
        
            :raises ValueOutOfRange: Raised when values are too big to get divided by one value.
        """
        while True:
            self.divider *= 10
            if math.isinf(self.divider):
                raise ValueOutOfRange()
            try:
                tmpX = [self.valuesX[i]/10 for i in range(0, self.len)]
                tmpY = [self.valuesY[i]/10 for i in range(0, self.len)]
                self.valuesX = tmpX
                self.valuesY = tmpY
                self.sumX = np.sum(self.valuesX)
                self.sumY = np.sum(self.valuesY)
                self.sumXxY = np.sum([self.valuesX[i]*self.valuesY[i] for i in range(0, self.len)])
                self.sumXxX = np.sum([self.valuesX[i]*self.valuesX[i] for i in range(0, self.len)])
                if self.someoneIsInf():
                    raise ValueOutOfRange()
            except:
                continue
            break

    def someoneIsInf(self):
        """
            Returns True if a value in this object is infinite
        
            :return: True if a value is infinite, False if not
            :rtype: Bool 
        """
        if math.isinf(self.sumXxX) or math.isinf(self.sumXxY) or math.isinf(self.sumY) or math.isinf(self.sumX):
            return True
        for i in range(0, self.len):
            if math.isinf(self.valuesX[i]) or math.isinf(self.valuesY[i]):
                return True
        return False
