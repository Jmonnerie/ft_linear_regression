# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    priceOf.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/29 18:52:17 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/10 04:31:28 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

"""
    This module prints out the Y value, according to the theta values in ./data/thetaValues.txt

    Example:
    ``` shell
        $ python3.7 ./sample/priceOf.py 150000 # I want the price of a 150000 mileage car
        The average price of a car with a mileage of 150000 km is 5290 euro
    ```

    :raise UsageError: When problem encountered with arguments
    :raise ThetaValuesCorrupted: When problem encountered with thetaValues.txt
    :raise ValueError: When values sended to getPriceOf are not numbers
"""

import strings
import sys
import utils
from exceptions import UsageError, ThetaValuesCorrupted


def getPriceOf(theta0, theta1, mileage):
    """
        Y = theta0 + theta1 * valueX is a linear function. This function returns Y

        :param theta0: Intercept
        :param theta1: Slope
        :param valueX: Obvious
        :return: value of Y
        :raise ValueError: When values sended are not numbers
    """
    return theta1 * mileage + theta0


def main(mileage=0):
    """
        This function read thetaValues.txt and print the price.

        :param mileage: number
        :raise UsageError: When mileage is not a number
        :raise ThetaValuesCorrupted: When a problem occurs with thetaValues.txt
    """
    if not utils.is_number(mileage):
        print(strings.paramNotANumber)
        raise UsageError()
    # Get informations saved in thetaValues.txt file
    f = open("./data/thetaValues.txt", "r")
    thetaInfos = f.read()
    f.close()
    t0match = strings.t0regex.search(thetaInfos)
    t1match = strings.t1regex.search(thetaInfos)
    if t0match is None or t1match is None:
        raise ThetaValuesCorrupted()
    theta0 = float(t0match.group(1))
    theta1 = float(t1match.group(1))
    mileage = float(mileage)
    price = getPriceOf(theta0, theta1, mileage)  # Calc the price.
    print(strings.outputPrice % (mileage, price))  # Print it.


if __name__ == "__main__":
    try:
        if len(sys.argv) == 2:
            main(sys.argv[1])
        else:
            raise UsageError()
    except UsageError:
        print(strings.priceOfUsage)
    except ThetaValuesCorrupted:
        print(strings.thetaValuesCorrupted)
    except IOError:
        print(strings.thetaValuesMissing)
