# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    strings.py                                         :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/29 04:35:19 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/09 18:11:34 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import re

# main program
usage = """Usage:
$ python3.7 ./sample/main.py [param]
Where param =
  - train [optionalfileName1] ... [optional_fileNameN]: launch the training mode with dataFiles in parameter or with ./data/data.csv
  - visu  [optionalfileName1] ... [optional_fileNameN]: launch train with dataFiles in parameter or with ./data/data.csv and open the visualisator
  - getPriceOf [mileage]: get the price of a car according to his mileage"""

# priceOf.py
priceOfUsage = """Usage:
$ python3.7 ./sample/priceOf.py [mileage]
Where mileage is a number"""
outputPrice = "The average price of a car with a mileage of %d km is %d euro"
t0regex = re.compile(r"^theta0=([-+]?\d+(?:\.\d*)?)$", re.MULTILINE)
t1regex = re.compile(r"^theta1=([-+]?\d+(?:\.\d*)?)$", re.MULTILINE)

# train.py
trainUsage = """Usage:
$ python3.7 ./sample/train.py [optionalFile1]...[optionalFileN]
Where datas in optionalFiles are on this format (if multiple files, ValueNames have to be the same through all files):
xValueName,yValueName
xVal0,yVal0
xVal1,yVal1
xVal2,yVal2
...
xValN,yValN"""

# visu.py
visuUsage = """Usage:
$ python3.7 ./sample/visu.py [optionalFile1]...[optionalFileN]
Where datas in optionalFiles are on this format (if multiple files, ValueNames have to be the same through all files):
xValueName,yValueName
xVal0,yVal0
xVal1,yVal1
xVal2,yVal2
...
xValN,yValN"""

# Errors/Warnings strings
thetaValuesMissing = "Error: No calculated data for price of a car by mileage found, please launch the training mode."
thetaValuesCorrupted = "Error: File ./data/thetaValues.txt has been corrupted. Launch the training mode to repair."
fileNotFound = "Error: File '%s' not found."
nameError = "Error: Names in '%s' file are not well formated"
parseValueError = "Error: Values in '%s' file line %d is not well formated"
valueNamesNotEqual = "Error: Value names in '%s' file are not equal to others (%s,%s != %s,%s)"
valueOutOfRange = "Error during process: values in dataset are too much big."
valuePriceOfOutOfRange = "Error: Value asked for priceOf module is too much big."
valuesNotGood = "Error: A value in dataset is not a number."
paramNotANumber = "Error: The parameter send is not a number."
numberNotReal = "Error: The data value in %s file line %d is not a real number"
dsIsnotDataset = "Error: The params send to main in train.py are not of the good type"

# files
defaultFile = "./data/data.csv"
thetaValuesFile = "./data/thetaValues.txt"