# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    main.py                                            :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/28 03:19:33 by jojomoon          #+#    #+#              #
#    Updated: 2020/04/10 04:22:31 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

"""
	Main module for the linear regression project
		
	Launch different modes:
	- train: train the model with data files in arguments or with ./data.data.csv by default.
	- visu: launch the training mode with a visualisator.
	- getPriceOf: get the value Y according to the theta0 and theta1 in ./data/thetaValues.txt, filled by the training mode.

	:raise UsageError: Displays help when usage is bad
	:raise ValueOutOfRange: When a value is too big.
	:raise ValueNotGood: When a value in the dataset is not a number.
"""

import sys
from exceptions import UsageError, ValueOutOfRange, ValueNotGood, ThetaValuesCorrupted
import strings
import priceOf
import visu
import train
import parse

def main() :
	try:
		lenArgv = len(sys.argv)
		if lenArgv == 1:  # Throws the usage when no parameter is passed to the program
			raise UsageError()
		if lenArgv == 3 and sys.argv[1] == "getPriceOf":  # Launch the getPriceOf mode
			priceOf.main(sys.argv[2])
		elif lenArgv >= 2 and sys.argv[1] == "visu":  # Launch the visualisator mode
			ds = parse.main()
			visu.main(ds)
		elif lenArgv >= 2 and sys.argv[1] == "train":  # Launch the training mode
			ds = parse.main()
			train.main(ds, False)
		else:
			raise UsageError()  # Throws a usageError when parameters are not good
	except UsageError:
		print(strings.usage)
	except IOError:
		print(strings.thetaValuesMissing)
	except ValueOutOfRange:
		print(strings.valueOutOfRange)
	except ValueNotGood:
		print(strings.valuesNotGood)
	except ThetaValuesCorrupted:
		print(strings.thetaValuesCorrupted)


if __name__ == "__main__" :
	main()