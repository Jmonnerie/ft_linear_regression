# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    outputMainParse.py                                 :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/31 21:24:56 by jojomoon          #+#    #+#              #
#    Updated: 2020/03/31 21:27:04 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import os
import sys
# Get current path
current = os.path.dirname(os.path.abspath(__file__))
# Get parent path
parent = os.path.dirname(current)
# Add parent to python paths
sys.path.append(parent)
# Add sample to python paths
sys.path.append(parent + "/sample")
from sample import parse

if __name__ == "__main__":
  print(parse.main())