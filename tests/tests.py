# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    tests.py                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/29 04:47:45 by jojomoon          #+#    #+#              #
#    Updated: 2020/07/28 11:26:16 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

import unittest
from unittest.mock import patch
import os
import sys
import io
# Get current path
current = os.path.dirname(os.path.abspath(__file__))
# Get parent path
parent = os.path.dirname(current)
# Add parent to python paths
sys.path.append(parent)
# Add sample to python paths
sys.path.append(parent + "/sample")
from sample import strings, priceOf, parse, utils, train, dataset, exceptions
from exceptions import ValueNotGood, UsageError, DatasetNotEqual

class TestStringMethods(unittest.TestCase):

# main.py 
  # Errors
    def test_main_usageError_getPriceOf_noMileage(self):
        with os.popen('python3 ./sample/main.py getPriceOf') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.usage + "\n")

    def test_main_usageError_getPriceOf_badMileage(self):
        with os.popen('python3 ./sample/main.py getPriceOf nan') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.paramNotANumber + "\n" + strings.usage + "\n")

    def test_main_usageError_visu_noDataFileFound(self):
        # Rename data.csv if exists
        exist = os.path.exists("./data/data.csv")
        if exist:
            os.rename("./data/data.csv","./data/data_tmp")
        # Test
        with os.popen('python3 ./sample/main.py visu') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.fileNotFound % strings.defaultFile + "\n" + strings.usage + "\n")
        # Rename back data.csv
        if exist:
            os.rename("./data/data_tmp","./data/data.csv")

    def test_main_usageError_train_noDataFileFound(self):
        # Rename data.csv if exists
        exist = os.path.exists("./data/data.csv")
        if exist:
            os.rename("./data/data.csv","./data/data_tmp")
        # Test
        with os.popen('python3 ./sample/main.py train') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.fileNotFound % strings.defaultFile + "\n" + strings.usage + "\n")
        # Rename back data.csv
        if exist:
            os.rename("./data/data_tmp","./data/data.csv")
            
    def test_main_usageError_randomParam(self):
        with os.popen('python3 ./sample/main.py randomparameter') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.usage + "\n")

    def test_main_usageError_tooMuchParam(self):
        with os.popen('python3 ./sample/main.py getPriceOf visu train') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.usage + "\n")

    # ThetaValues.txt is missing/corrupted
    def test_main_IOError_priceOf_thetaValuesMissing(self):
        # Rename thetaValues.txt if exists
        exist = os.path.exists("./data/thetaValues.txt")
        if exist:
            os.rename("./data/thetaValues.txt","./data/tmp")
        # Test
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesMissing + "\n")
        # Rename back thetaValues.txt
        if exist:
            os.rename("./data/tmp","./data/thetaValues.txt")
    
    def test_main_IOError_priceOf_thetaValuesCorrupted(self):
        # Rename thetaValues.txt if exists
        exist = os.path.exists("./data/thetaValues.txt")
        if exist:
            os.rename("./data/thetaValues.txt","./data/tmp")
        # Case1: empty theta
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\ntheta0=")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Case2: theta missing
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\n")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Case3: theta is not a number
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\ntheta0=NotANumber")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Case4: thetas on the same line
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42 theta0=101")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Rename back thetaValues.txt
        if exist:
            os.rename("./data/tmp","./data/thetaValues.txt")

    # Functionnality
    def test_main_priceOf_functionnalTest(self):
        # Rename thetaValues.txt if exists
        exist = os.path.exists("./data/thetaValues.txt")
        if exist:
            os.rename("./data/thetaValues.txt","./data/tmp")
        # Case: specific theta values
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\ntheta0=101")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10142') as stream:
            output = stream.read()
        stream.close()
        price = 42 * 10142 + 101
        self.assertEqual(output, strings.outputPrice % (10142, price) + "\n")
        os.remove("./data/thetaValues.txt")
        # Rename back thetaValues.txt
        if exist:
            os.rename("./data/tmp","./data/thetaValues.txt")
    
# visu.py # Done
    # Usage
    def test_usageVisu_noDataFileFound(self):
        # Rename data.csv if exists
        exist = os.path.exists("./data/data.csv")
        if exist:
            os.rename("./data/data.csv","./data/data_tmp")
        # Test
        with os.popen('python3 ./sample/visu.py') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.fileNotFound % strings.defaultFile + "\n" + strings.visuUsage + "\n")
        # Rename back data.csv
        if exist:
            os.rename("./data/data_tmp","./data/data.csv")  

# train.py # DONE
  # Errors
    def test_train_UsageError_noDataFileFound(self):
        # Rename data.csv if exists
        exist = os.path.exists("./data/data.csv")
        if exist:
            os.rename("./data/data.csv","./data/data_tmp")
        # Test
        with os.popen('python3 ./sample/train.py') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.fileNotFound % strings.defaultFile + "\n" + strings.trainUsage + "\n")
        # Rename back data.csv
        if exist:
            os.rename("./data/data_tmp","./data/data.csv")

    def test_train_main_TypeError(self):
        with self.assertRaises(TypeError):
            train.main(dataset.Dataset([1,2], [1,2], "n1", "n2"), "lols")

  # Unit Tests
    def test_train_main(self):
        km = ['km', 240000.0, 139800.0, 150500.0, 185530.0, 176000.0, 114800.0, 166800.0, 89000.0, 144500.0, 84000.0, 82029.0, 63060.0, 74000.0, 97500.0]
        price = ['price', 3650.0, 3800.0, 4400.0, 4450.0, 5250.0, 5350.0, 5800.0, 5990.0, 5999.0, 6200.0, 6390.0, 6390.0, 6600.0, 6800.0]
        ds = dataset.Dataset(km[1:], price[1:], km[0], price[0])
        train.main(ds, False)
        fd = open(strings.thetaValuesFile, "r")
        output = fd.read()
        fd.close()
        self.assertEqual(output, "theta0=7494.130330\ntheta1=-0.015485")

    def test_train_save(self):
        train.save(42, 101)
        fd = open(strings.thetaValuesFile, "r")
        output = fd.read()
        fd.close()
        self.assertEqual(output, "theta0=%f\ntheta1=%f" % (float(42), float(101)))

    def test_train_sumTmpT0(self):
        km = [1,2,3,4,5]
        price = [6,7,8,9,10]
        t0 = 101
        t1 = 42
        sKm = 0
        sPrice = 0
        for idx in range(0, len(km)):
            sKm += km[idx]
            sPrice += price[idx]
        ds = dataset.Dataset(km, price, "km", "price")
        self.assertEqual(train.sumTmpT0(ds, t0, t1), t0*len(km)+t1*sKm-sPrice)

    def test_train_sumTmpT1(self):
        km = [1,2,3,4,5]
        price = [6,7,8,9,10]
        t0 = 101
        t1 = 42
        sKm = 0
        sPrice = 0
        sKmxPrice = 0
        sKm2= 0
        for idx in range(0, len(km)):
            sKm += km[idx]
            sPrice += price[idx]
            sKmxPrice += km[idx]*price[idx]
            sKm2 += km[idx]*km[idx]
        ds = dataset.Dataset(km, price, "km", "price")
        self.assertEqual(train.sumTmpT1(ds, t0, t1), t0*sKm+t1*sKm2-sKmxPrice)

# priceOf.py # DONE
  # Errors
    # UsageError
    def test_priceOf__UsageErrornotGoodNumberOfParams(self):
        with os.popen('python3 ./sample/priceOf.py') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.priceOfUsage + "\n")
        with os.popen('python3 ./sample/priceOf.py 12354 431345') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.priceOfUsage + "\n")

    def test_priceOf_UsageError_paramNotANumber(self):
        with os.popen('python3 ./sample/priceOf.py nan') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.paramNotANumber + "\n" + strings.priceOfUsage + "\n")
        with os.popen('python3 ./sample/priceOf.py inf') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.paramNotANumber + "\n" + strings.priceOfUsage + "\n")
        with os.popen('python3 ./sample/priceOf.py String') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.paramNotANumber + "\n" + strings.priceOfUsage + "\n")

    # IOError
    def test_priceOf_IOError_thetaValuesMissing(self):
        # Rename thetaValues.txt if exists
        exist = os.path.exists("./data/thetaValues.txt")
        if exist:
            os.rename("./data/thetaValues.txt","./data/tmp")
        # Test
        with os.popen('python3 ./sample/priceOf.py 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesMissing + "\n")
        # Rename back thetaValues.txt
        if exist:
            os.rename("./data/tmp","./data/thetaValues.txt")
    
    def test_priceOf_IOError_thetaValuesCorrupted(self):
        # Rename thetaValues.txt if exists
        exist = os.path.exists("./data/thetaValues.txt")
        if exist:
            os.rename("./data/thetaValues.txt","./data/tmp")
        # Case1: empty theta
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\ntheta0=")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Case2: theta missing
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\n")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Case3: theta is not a number
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\ntheta0=NotANumber")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Case4: thetas on the same line
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42 theta0=101")
        fd.close()
        with os.popen('python3 ./sample/main.py getPriceOf 10000') as stream:
            output = stream.read()
        stream.close()
        self.assertEqual(output, strings.thetaValuesCorrupted + "\n")
        os.remove("./data/thetaValues.txt")
        # Rename back thetaValues.txt
        if exist:
            os.rename("./data/tmp","./data/thetaValues.txt")

    # ValueError
    def test_getPriceOf_ValueError_badParam(self):
        with self.assertRaises(TypeError):
            priceOf.getPriceOf("string", 42, 42101)

  # Unit tests
    def test_priceOf_getPriceOf(self):
        output = priceOf.getPriceOf(42, 101, 42101)
        price = 42 + 101 * 42101
        self.assertEqual(price, output)

    def test_priceOf_main(self):
        # Rename thetaValues.txt if exists
        exist = os.path.exists("./data/thetaValues.txt")
        if exist:
            os.rename("./data/thetaValues.txt","./data/tmp")
        # Case: specific theta values
        fd = open("./data/thetaValues.txt","w+")
        fd.write("theta1=42\ntheta0=101")
        fd.close()
        with os.popen('python3 ./sample/priceOf.py 10142') as stream:
            output = stream.read()
        stream.close()
        price = 42 * 10142 + 101
        self.assertEqual(output, strings.outputPrice % (10142, price) + "\n")
        os.remove("./data/thetaValues.txt")
        # Rename back thetaValues.txt
        if exist:
            os.rename("./data/tmp","./data/thetaValues.txt")

# parse.py # DONE
  # Errors
    def test_parse_UsageError_valueNamesNotEqual(self):
        fd = open("./data/dataTest1.csv","w+")
        fd.write("km,price\n240000,3650\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        fd = open("./data/dataTest2.csv","w+")
        fd.write("notGoodName,price\n240000,3650\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        # Tests
        testargs = ["file", "./data/dataTest1.csv", "./data/dataTest2.csv"]
        with patch.object(sys, 'argv', testargs):
            outputCapturer = io.StringIO()
            sys.stdout = outputCapturer
            with self.assertRaises(UsageError):
                parse.main()
        sys.stdout = sys.__stdout__
        self.assertEqual(outputCapturer.getvalue(), strings.valueNamesNotEqual % ("./data/dataTest2.csv", "km", "price", "notGoodName", "price") + "\n")
        # Delete test dataFile
        os.remove("./data/dataTest1.csv")
        os.remove("./data/dataTest2.csv")

    def test_parse_UsageError_fileNotFound(self):
        # Tests
        with self.assertRaises(UsageError):
            outputCapturer = io.StringIO()
            sys.stdout = outputCapturer
            parse.getFileData("test")
        sys.stdout = sys.__stdout__
        self.assertEqual(outputCapturer.getvalue(), strings.fileNotFound % ("test") + "\n")

    def test_parse_UsageError_nameError(self):
        # Create new dataFile
        fd = open("./data/dataTest.csv","w+")
        fd.write("kmPrice\n240000,3650\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        # Tests
        with self.assertRaises(UsageError):
            outputCapturer = io.StringIO()
            sys.stdout = outputCapturer
            parse.getFileData("./data/dataTest.csv")
        sys.stdout = sys.__stdout__
        self.assertEqual(outputCapturer.getvalue(), strings.nameError % ("./data/dataTest.csv") + "\n")
        # Delete test dataFile
        os.remove("./data/dataTest.csv")

    def test_parse_UsageError_parseValueError(self):
      # Case 1: String
        # Create new dataFile
        fd = open("./data/dataTest.csv","w+")
        fd.write("Km,Price\n240000,born2code\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        # Tests
        with self.assertRaises(UsageError):
            outputCapturer = io.StringIO()
            sys.stdout = outputCapturer
            parse.getFileData("./data/dataTest.csv")
        sys.stdout = sys.__stdout__
        self.assertEqual(outputCapturer.getvalue(), strings.parseValueError % ("./data/dataTest.csv", 2) + "\n")
        # Delete test dataFile
        os.remove("./data/dataTest.csv")
      # Case 2: InfiniteValue
        fd = open("./data/dataTest.csv","w+")
        fd.write("Km,Price\n240000,9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        # Tests
        with self.assertRaises(UsageError):
            outputCapturer = io.StringIO()
            sys.stdout = outputCapturer
            parse.getFileData("./data/dataTest.csv")
        sys.stdout = sys.__stdout__
        self.assertEqual(outputCapturer.getvalue(), strings.numberNotReal % ("./data/dataTest.csv", 2) + "\n")
        # Delete test dataFile
        os.remove("./data/dataTest.csv") 

  # Unit Tests
    def test_parse_mainWithoutParameters(self):
        exist = os.path.exists("./data/data.csv")
        if exist:
            os.rename("./data/data.csv","./data/data_tmp")
        fd = open("./data/data.csv","w+")
        fd.write("km,price\n240000,3650\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        with os.popen('python3 ./tests/outputMainParse.py') as stream:
            output = stream.read()
        stream.close()
        km = [240000.0, 139800.0, 150500.0, 185530.0, 176000.0, 114800.0, 166800.0]
        price = [3650.0, 3800.0, 4400.0, 4450.0, 5250.0, 5350.0, 5800.0]
        self.assertEqual(output, "Dataset price in founction of km (7 values):\n- km: %s\n- price: %s\n- sumX: 1173430.0\n- sumY: 32700.0\n- sumXxX: 206192950900.0\n- sumXxY: 5400668500.0\n" % (km, price))
        os.remove("./data/data.csv")
        if exist:
            os.rename("./data/data_tmp","./data/data.csv")

    def test_parse_mainWithParameters(self):
        # Rename data1.csv and data2.csv if exists
        exist1 = os.path.exists("./data/data1.csv")
        exist2 = os.path.exists("./data/data2.csv")
        if exist1:
            os.rename("./data/data1.csv","./data/data1_tmp")
        if exist2:
            os.rename("./data/data2.csv","./data/data2_tmp")
        # Create new ones
        fd = open("./data/data1.csv","w+")
        fd.write("km,price\n240000,3650\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        fd = open("./data/data2.csv","w+")
        fd.write("km,price\n89000,5990\n")
        fd.close()
        # Test
        with os.popen('python3 ./tests/outputMainParse.py ./data/data1.csv ./data/data2.csv') as stream:
            output = stream.read()
        stream.close()
        km = [240000.0, 139800.0, 150500.0, 185530.0, 176000.0, 114800.0, 166800.0, 89000.0]
        price = [3650.0, 3800.0, 4400.0, 4450.0, 5250.0, 5350.0, 5800.0, 5990.0]
        self.assertEqual(output, "Dataset price in founction of km (8 values):\n- km: %s\n- price: %s\n- sumX: 1262430.0\n- sumY: 38690.0\n- sumXxX: 214113950900.0\n- sumXxY: 5933778500.0\n" % (km, price))
        os.remove("./data/data1.csv")
        os.remove("./data/data2.csv")
        # Rename back data1.csv and data2.csv
        if exist1:
            os.rename("./data/data1_tmp","./data/data1.csv")
        if exist2:
            os.rename("./data/data2_tmp","./data/data2.csv")
    
    def test_parse_getfileData(self):
        # Rename data.csv if exists
        exist = os.path.exists("./data/data.csv")
        if exist:
            os.rename("./data/data.csv","./data/data_tmp")
        # Create new one
        fd = open("./data/data.csv","w+")
        fd.write("km,price\n240000,3650\n139800,3800\n150500,4400\n185530,4450\n176000,5250\n114800,5350\n166800,5800\n")
        fd.close()
        # Test
        outputKm, outputPrice = parse.getFileData("./data/data.csv")
        km = ["km", 240000.0, 139800.0, 150500.0, 185530.0, 176000.0, 114800.0, 166800.0]
        price = ["price", 3650.0, 3800.0, 4400.0, 4450.0, 5250.0, 5350.0, 5800.0]
        self.assertEqual(outputKm, km)
        self.assertEqual(outputPrice, price)
        os.remove("./data/data.csv")
        # Rename back data.csv
        if exist:
            os.rename("./data/data_tmp","./data/data.csv")

# utils.py # Done
  # Errors
    def test_utils_findTheBiggerIdx_TypeError(self):
        tab = [1,2,3,4,5,"six",7]
        with self.assertRaises(TypeError):
            utils.findTheBiggerIdx(tab)
            
  # Unit Tests
    def test_utils_findTheBiggerIdX(self):
        tab = [1,2,3,4,5,-60,7,8,9,0,15,18,2,15,18,10,-20]
        idx_max = utils.findTheBiggerIdx(tab)
        self.assertEqual(11, idx_max)

    def test_utils_is_number(self):
        tabTest = ["asdf", "nan", "42", "101.42", "101.42str"]
        retExpected = [False, False, True, True, False]
        for i in range(0, len(tabTest)):
            self.assertEqual(retExpected[i], utils.is_number(tabTest[i]))

# dataset.py # Done
  # Errors
    def test_dataset_ValueNotGood(self):
        valY = [9999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999999, 1, 2, 3, 4, 5]
        valX = [1,2,3,4,5,6]
        with self.assertRaises(ValueNotGood):
            dataset.Dataset(valX, valY, "name1", "name2")

    def test_dataset_DatasetNotEqual(self):
        valX = [1,2,3]
        valY = [1,2,3,4]
        with self.assertRaises(DatasetNotEqual):
            dataset.Dataset(valX, valY, "name1", "name2")

  # Unit tests
    def test_dataset_normalValues(self):
        valX = [1,2,3,4]
        valY = [1,2,3,4]
        dataset.Dataset(valX, valY, "n1", "n2")

    def test_dataset_bigValues(self):
        string = "9"
        while utils.is_number(string + "9"):
            string += "9"
        valX = [string for i in range(0, 1000)]
        valY = [string for i in range(0, 1000)]
        dataset.Dataset(valX, valY, "km", "price")
    
    def test_dataset_bigRangeValues(self):
        numLow = "1"
        numHigh = "9"
        while utils.is_number(numHigh + "9") and float("0.0" + numLow) != 0:
            numHigh += "9"
            numLow = "0" + numLow
        numLow = "0." + numLow
        valX = [numLow, numHigh]
        valY = [numLow, numHigh]
        dataset.Dataset(valX, valY, "km", "price")

    def test_dataset_someoneIsInf(self):
        ds = dataset.Dataset([1,2,3,4], [1,2,3,4], "name1", "name2")
        self.assertFalse(ds.someoneIsInf())
        ds.valuesX[0] = float("inf")
        self.assertTrue(ds.someoneIsInf())
        ds.valuesX[0] = 1
        ds.valuesY[0] = float("inf")
        self.assertTrue(ds.someoneIsInf())
        ds.valuesY[0] = 1
        ds.sumX = float("inf")
        self.assertTrue(ds.someoneIsInf())
        ds.sumX = 1
        ds.sumY = float("inf")
        self.assertTrue(ds.someoneIsInf())
        ds.sumY = 1
        ds.sumXxX = float("inf")
        self.assertTrue(ds.someoneIsInf())
        ds.sumXxX = 1
        ds.sumXxY = float("inf")
        self.assertTrue(ds.someoneIsInf())

    def test_dataset_updateDivider(self):
        valX = [10,20,30,40]
        valY = [10,20,30,40]
        ds = dataset.Dataset(valX, valY, "name1", "name2")
        ds.updateDivider()
        self.assertEqual(ds.valuesX, [1,2,3,4])
        self.assertEqual(ds.valuesY, [1,2,3,4])

    def test_dataset_setRangeX(self):
        valX = [10,20,30,40]
        valY = [10,20,30,39]
        ds = dataset.Dataset(valX, valY, "name1", "name2")
        ds.setRangeX()
        self.assertEqual(ds.rangeX, [0, 40])
        pass
    
    def test_dataset_setIdxMax(self):
        valX = [10,20,30,40]
        valY = [10,20,30,39]
        ds = dataset.Dataset(valX, valY, "name1", "name2")
        ds.setIdxMax()
        self.assertEqual(ds.idxMax, 3)
        self.assertEqual(ds.xMax, 40)
        pass
    
unittest.main()