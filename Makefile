# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jojomoon <jojomoon@student.42lyon.fr>      +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/03/28 03:11:41 by jojomoon          #+#    #+#              #
#    Updated: 2020/07/28 11:14:31 by jojomoon         ###   ########lyon.fr    #
#                                                                              #
# **************************************************************************** #

.PHONY: all install train visu_train price_of tests

all: train

help:
	@python3 ./sample/main.py help

install:
	@pip3 install -r requirements.txt

train:
	@python3 ./sample/main.py train

visu_train:
	@python3 ./sample/main.py visu

price_of:
	@python3 ./sample/main.py getPriceOf $(filter-out $@,$(MAKECMDGOALS))

%:
	@:

tests:
	@python3 tests/tests.py -v

test_one:
	@python3 tests/tests.py -v TestStringMethods.$(filter-out $@,$(MAKECMDGOALS))